const express = require('express');
const config = {
    name: 'express-app',
    port: 3000,
    host: '0.0.0.0',
};

const app = express();

//Request URL domain
app.get('/', (req, res) => {
    res.status(200).send('Express App Launched');
});

//Listening to the port and host based on config
app.listen(config.port, config.host, (e) => {
    if (e) {
        throw new Error('Internal Server Error');
    }
    console.log(`${config.name} is running on ${config.host}:${config.port}`);
});