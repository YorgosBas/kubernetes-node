A simple containerised Express.js application with Docker and orchestration with Kubernetes.

The Docker image of the application is saved into a tar archive that’s pushed into the Kubernetes image repository. A YAML configuration file takes this image and creates a container inside of a pod in Kubernetes and finally deploys the application with the appropriate services and registries

The autoscale application is in PHP to test the response of Kubernetes in increased load in order to
deploy more Pods and vice versa

## Installation
```yaml
git clone https://gitlab.com/YorgosBas/kubernetes-node.git
```
## Docker
Build the image
```yaml
docker build . -t localhost:32000/server:v1
```
Check if the app is running correctly
```yaml
docker run -p 3000:3000 localhost:32000/server:v1
```
Extract the image into an archive to be pushed into the Kubernetes image repository
```yaml
docker save localhost:32000/server:v1 > server.tar
```
## Kubernetes
Start the Kubernetes cluster with DNS and Registry enabled

Import the archive into kubernetes
```yaml
microk8s.ctr image import server.tar
```

Apply the configurations
```yaml
microk8s.kubectl apply -f node-pod.yaml
microk8s.kubectl apply -f node-deployment.yaml
microk8s.kubectl apply -f node-service.yaml
```
Enter the pod to make changes to the app
```yaml
microk8s.kubectl exec -it api-pod /bin/bash
```
Run the app from the pod
```yaml
microk8s.kubectl port-forward api-pod 3000:3000
```
Search ip and ports in which your pods and apps are running
```yaml
mocrok8s.kubectl get all --all-namespaces
```

## Debugging
In case you run into problems where you can't see the services correctly you can try the following solutions

Enable port fowarding
```yaml
sudo iptables -P FORWARD ACCEPT
```
Allow incoming and outgoing traffic
```yaml
sudo ufw allow in on cbr0 && sudo ufw allow out on cbr0
```
Enable routing protocol
```yaml
sudo ufw default allow routed
```
Restart docker
```yaml
sudo systemctl restart docker
```

## Autoscaling Kubernetes

Build the image inside autoscale app
```yaml
docker build -t localhost:32000/php-app:v1
```
Push the image to the registry
```yaml
docker push localhost:32000/php-app
```
Apply the configuration
```yaml
microk8s.kubectl apply -f php-autoscale.yaml
```
Run horizontal pod autoscaler
```yaml
microk8s.kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
Enable metrics server
```yaml
microk8s.enable metrics-server
```
Check the autoscaler status
```yaml
microk8s.kubectl get hpa
```
Create a pod with a one time interactive terminal and run the app
```yaml
microk8s.kubectl run --generator=run-pod/v1 -it --rm load-generator --image=busybox /bin/sh
```
Create a temporary container to run the mysql service
```yaml
microk8s.kubectl run -it --rm --image=mysql:8.0.3 --restart=Never mysql-client -- mysql -h mysql -ppassword